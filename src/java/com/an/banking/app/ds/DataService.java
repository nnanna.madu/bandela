package com.an.banking.app.ds;

import javax.annotation.PostConstruct;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import nw.commons.NeemClazz;
import nw.orm.core.service.Nworm;

/**
 * Data service operations class
 * @author Nnanna
 * @since 29 Sep 2017, 09:34:38
 */
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public abstract class DataService extends NeemClazz {
	protected Nworm dbService;

	@PostConstruct
	public void init(){
		dbService = Nworm.getInstance();
	}
	
	public Nworm getDbService(){
		return this.dbService;
	}

	public boolean update(Object obj){
		return dbService.update(obj);
	}
	
	public boolean save(Object obj){
		return dbService.create(obj) != null;
	}
}
