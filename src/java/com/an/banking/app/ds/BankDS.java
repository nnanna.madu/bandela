package com.an.banking.app.ds;

import java.util.Date;

import javax.ejb.Stateless;

import org.hibernate.criterion.Restrictions;

import com.an.banking.model.entity.BankAccount;
import com.an.banking.model.entity.BankUser;
import com.an.banking.model.enums.AccountType;
/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 14:40:30
 */
@Stateless
public class BankDS extends DataService {
	
	/**
	 * sets up default {@link BankUser} & {@link BankAccount}
	 */
	public void setupDefaultUser(){
		logger.debug("Checking for default user in the db...");
		String defaultEmail = "nnanna.madu@gmail.com";
		BankUser user = dbService.getByCriteria(BankUser.class, Restrictions.eq("email", defaultEmail));
		if(user == null){
			logger.debug("Creating default user...");
			user = new BankUser();
			user.setFirstName("Nnanna");
			user.setLastName("Madu");
			user.setEmail(defaultEmail);
			user.setPhone("08149573029");
			user.setAddress("Victoria Garden City");
			user.setBirthday(new Date());
			
			save(user);
			
//			setupDefaultAccount(user);
		}else{
			logger.debug("Default User is not null!");
		}
	}
	
	public void setupDefaultAccount(BankUser user){
		logger.debug("Setting up default account...");
		BankAccount account = dbService.getByCriteria(BankAccount.class, Restrictions.eq("accountNumber", "2011044959"));
		if(account == null){
			logger.debug("Creating default account...");
			account = new BankAccount();
			account.setAccountNumber("2011044959");
			account.setAccountType(AccountType.SAVINGS);
			account.setCurrentBalance(100000.0);
			account.setPin("2090");
			account.setUser(user);
			
			save(account);
		}else{
			logger.debug("Default account is not null");
		}
	}
}
