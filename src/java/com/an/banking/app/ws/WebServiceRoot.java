/**
 * 
 */
package com.an.banking.app.ws;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 10:47:00
 */
@ApplicationPath("/api")
public class WebServiceRoot extends Application {

}
