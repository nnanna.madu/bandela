package com.an.banking.app.ws.requests;

import com.google.gson.Gson;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 15:47:39
 */
public class NewAccountRequest {
	private String firstName;
	private String lastName;
	private String email;
	private String accountNo;
	private String phoneNo;
	private String address;
	private Long birthday;
	private String pin;
	private String accountType;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Long getBirthday() {
		return birthday;
	}
	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public static void main(String[] args) {
		NewAccountRequest acct = new NewAccountRequest();
		acct.setAccountNo("2011044959");
		acct.setAccountType("SAVINGS");
		acct.setBirthday(System.currentTimeMillis());
		acct.setEmail("nnanna.madu@gmail.com");
		acct.setFirstName("Nnanna");
		acct.setLastName("Madu");
		acct.setPhoneNo("08149573029");
		acct.setPin("2090");
		
		System.out.println(new Gson().toJson(acct));
	}
}
