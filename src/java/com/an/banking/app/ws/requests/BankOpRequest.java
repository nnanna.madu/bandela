package com.an.banking.app.ws.requests;

import com.google.gson.Gson;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 17:47:08
 */
public class BankOpRequest {
	private String accountNo;
	private double amount;
	private String txnRef;
	private Long txnTime;
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTxnRef() {
		return txnRef;
	}
	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}
	public Long getTxnTime() {
		return txnTime;
	}
	public void setTxnTime(Long txnTime) {
		this.txnTime = txnTime;
	}
	
	public static void main(String[] args) {
		BankOpRequest req = new BankOpRequest();
		req.setAccountNo("2011044959");
		req.setAmount(5000.0);
		req.setTxnRef("AN-12345");
		req.setTxnTime(System.currentTimeMillis());
		
		System.out.println(new Gson().toJson(req));
	}
}