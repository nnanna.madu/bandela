package com.an.banking.app.ws.inquiries;

import javax.ejb.Stateless;

import nw.commons.BCrypt;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import com.an.banking.app.ds.DataService;
import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.app.ws.responses.BalanceInquiryResponse;
import com.an.banking.model.entity.BankAccount;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 17:03:34
 */
@Stateless
public class BalanceInquiryDataService extends DataService {
	/**
	 * Gets current balance in bank account
	 * @param accountNo
	 * @param pin
	 * @return
	 */
	public BalanceInquiryResponse getAccountBalance(String accountNo, String pin){
		//validate inputs
		if(StringUtils.isEmpty(accountNo) || StringUtils.isEmpty(pin)){
			return new BalanceInquiryResponse(ResponseCodeEnum.INVALID_INPUT, ResponseCodeEnum.INVALID_INPUT.getDescription());
		}

		BankAccount account = getDbService().getByCriteria(BankAccount.class, Restrictions.eq("accountNumber", accountNo));
		if(account == null){
			return new BalanceInquiryResponse(ResponseCodeEnum.ACCOUNT_NOT_FOUND, ResponseCodeEnum.ACCOUNT_NOT_FOUND.getDescription());
		}

		//validate pin
		if(!BCrypt.checkpw(pin, account.getPin())){
			return new BalanceInquiryResponse(ResponseCodeEnum.INVALID_PIN, ResponseCodeEnum.INVALID_PIN.getDescription());
		}

		BalanceInquiryResponse resp = new BalanceInquiryResponse(ResponseCodeEnum.SUCCESS, ResponseCodeEnum.SUCCESS.getDescription());
		resp.setBalance(account.getCurrentBalance());

		return resp;
	}
}
