package com.an.banking.app.ws.inquiries;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 16:39:49
 */
public interface IBalanceInquiryService {
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkBalance(@FormParam("accountNo") String accountNo, @FormParam("pin") String pin);
}
