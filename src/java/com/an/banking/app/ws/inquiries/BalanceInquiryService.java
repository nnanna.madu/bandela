package com.an.banking.app.ws.inquiries;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.an.banking.app.ws.responses.BalanceInquiryResponse;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 16:48:50
 */
@Path("/inquiry")
public class BalanceInquiryService implements IBalanceInquiryService {
	
	@Inject
	BalanceInquiryDataService dataService;
	
	@Override
	public Response checkBalance(String accountNo, String pin) {
		BalanceInquiryResponse resp = dataService.getAccountBalance(accountNo, pin);
		return Response.ok(resp).build();
	}

}
