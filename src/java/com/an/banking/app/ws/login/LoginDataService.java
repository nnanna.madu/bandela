package com.an.banking.app.ws.login;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import nw.commons.BCrypt;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import com.an.banking.app.ds.DataService;
import com.an.banking.app.enums.ProjectProperty;
import com.an.banking.app.security.IntrusionAnalyzer;
import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.model.entity.BankAccount;
import com.an.banking.model.entity.BankUser;

/**
 * 
 * @author Nnanna
 * @since 30 Sep 2017, 17:31:31
 */
@Stateless
public class LoginDataService extends DataService {
	
	@Inject
	IntrusionAnalyzer analyzer;
	
	/**
	 * Validates user details & logs user in
	 * @param accountNo
	 * @param password
	 * @return {@link LoginResponse} response
	 */
	public LoginResponse handleUserLogin(String accountNo, String password){
		//validate form params
		if(StringUtils.isEmpty(accountNo) || StringUtils.isEmpty(password)){
			return new LoginResponse(ResponseCodeEnum.INVALID_INPUT, ResponseCodeEnum.INVALID_INPUT.getDescription());
		}
		
		//validate length of params
		if(!validateLength(accountNo, 10) || !validateLength(password, 10)){
			return new LoginResponse(ResponseCodeEnum.INVALID_INPUT, ResponseCodeEnum.INVALID_INPUT.getDescription());
		}
		
		//fetch user account by account number
		BankAccount account = getDbService().getByCriteria(BankAccount.class, Restrictions.eq("accountNumber", accountNo.trim()));
		if(account == null){
			return new LoginResponse(ResponseCodeEnum.ACCOUNT_NOT_FOUND, ResponseCodeEnum.ACCOUNT_NOT_FOUND.getDescription());
		}else{
			logger.info(accountNo + " is a valid account number!");
		}
		
		//extract pin from token
		String pin = StringUtils.substring(password, 0, 4);
		String hwToken = StringUtils.substring(password, 4, 10);
		
		//validate pin
		if(!BCrypt.checkpw(pin, account.getPin())){
			return new LoginResponse(ResponseCodeEnum.INVALID_PIN, ResponseCodeEnum.INVALID_PIN.getDescription());
		}
		
		//validate token
		String tokens = appProps.getProperty(ProjectProperty.ALLOWED_TOKENS.getKey(), ProjectProperty.ALLOWED_TOKENS.getDefaultValue());
		List<String> tokenList = Arrays.asList(tokens.split(","));
		if(tokenList != null && !tokenList.contains(hwToken)){
			return new LoginResponse(ResponseCodeEnum.INVALID_TOKEN, ResponseCodeEnum.INVALID_TOKEN.getDescription());
		}
		
		//construct login response
		LoginResponse resp = null;
		
		//fetch user 
		BankUser user = getDbService().getByCriteria(BankUser.class, Restrictions.idEq(account.getUser().getPk())); //user is initialized lazily in model level
		if(user != null){
			//construct response
			resp = new LoginResponse(ResponseCodeEnum.SUCCESS, ResponseCodeEnum.SUCCESS.getDescription());
			resp.setCurrentBalance(account.getCurrentBalance());
			resp.setUsername(user.getFirstName() + " " + user.getLastName());
		}else{
			resp = new LoginResponse(ResponseCodeEnum.USER_NOT_FOUND, ResponseCodeEnum.USER_NOT_FOUND.getDescription()); //this is highly unlikely though
		}
		return resp;
	}
	
	private boolean validateLength(String input, int expectedLength){
		return input.length() == expectedLength;
	}
}
