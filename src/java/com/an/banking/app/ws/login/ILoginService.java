package com.an.banking.app.ws.login;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 15:20:43
 */
public interface ILoginService {
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doLogin(@FormParam("accountNo") String accountNo, @FormParam("pw") String password);
}
