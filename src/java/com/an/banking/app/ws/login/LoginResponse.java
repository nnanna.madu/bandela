package com.an.banking.app.ws.login;

import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.app.ws.ResponseData;

public class LoginResponse extends ResponseData {
	private static final long serialVersionUID = 1L;
	private Double currentBalance;
	private String username;
	
	public LoginResponse(){}
	
	public LoginResponse(ResponseCodeEnum code, String description){
		setCode(code);
		setDescription(description);
	}

	public Double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(Double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
