package com.an.banking.app.ws.login;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import nw.commons.NeemClazz;

import com.an.banking.app.security.IntrusionAnalyzer;
import com.an.banking.app.ws.ResponseCodeEnum;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 15:20:51
 */
@Path("/login")
public class LoginService extends NeemClazz implements ILoginService {

	@Inject
	LoginDataService dataService;
	
	@Inject
	IntrusionAnalyzer analyzer;

	@Override
	@PermitAll
	public Response doLogin(String accountNo, String password) {
		LoginResponse resp = dataService.handleUserLogin(accountNo, password);
		if(resp.getCode() == ResponseCodeEnum.SUCCESS.getCode()){
			String jwt = analyzer.generateToken(resp.getUsername(), accountNo);
//			logger.debug("Generated token: " + jwt);
			return Response.ok(resp).header("jwt", jwt).build();
		}
		return Response.ok(resp).build();
	}

	
}
