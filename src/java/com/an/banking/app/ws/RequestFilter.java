package com.an.banking.app.ws;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import nw.commons.NeemClazz;

import org.apache.commons.lang.StringUtils;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethodInvoker;

import com.an.banking.app.security.IntrusionAnalyzer;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 10:46:47
 */
@Provider
@ServerInterceptor
public class RequestFilter extends NeemClazz implements ContainerRequestFilter {
	public static final String AUTHORIZATION_HEADER = "Authorization";

	@Context
	protected HttpServletRequest req;

	@Inject
	IntrusionAnalyzer analyzer;

	@Override
	public void filter(ContainerRequestContext request) throws IOException {
		ResourceMethodInvoker mi = (ResourceMethodInvoker) request.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");

		// bypass all permit all request
		if(mi.getMethod().isAnnotationPresent(PermitAll.class)){
			logger.debug("PermitAll Request");
			return;
		}

		//validate jwt token
		String jwt = resolveToken(req);
		logger.debug("JWT in Request Filter: " + jwt);
		if(StringUtils.isNotEmpty(jwt) && analyzer.validateToken(jwt)){
			logger.debug("Incoming request is valid");
			return;
		}else{
			request.abortWith(Response.status(Status.UNAUTHORIZED).entity("Unauthorized").build());
			return;
		}
	}

	private String resolveToken(HttpServletRequest request){
		String bearerToken = request.getHeader(AUTHORIZATION_HEADER);
		logger.debug("bearerToken: " + bearerToken);
		if (StringUtils.isNotEmpty(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

}
