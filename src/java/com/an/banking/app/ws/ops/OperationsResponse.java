package com.an.banking.app.ws.ops;

import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.app.ws.ResponseData;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 18:12:43
 */
public class OperationsResponse extends ResponseData {
	
	public OperationsResponse(){}
	
	public OperationsResponse(ResponseCodeEnum code, String description){
		setCode(code);
		setDescription(description);
	}
}
