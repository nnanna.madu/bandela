package com.an.banking.app.ws.ops;

import java.util.Date;

import com.an.banking.app.ds.DataService;
import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.app.ws.requests.BankOpRequest;
import com.an.banking.model.entity.BankAccount;
import com.an.banking.model.entity.TransactionLogs;
import com.an.banking.model.enums.TransactionType;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 18:11:41
 */
@Stateless
public class OperationsDataService extends DataService {

	public OperationsResponse logTransaction(BankOpRequest request, boolean deposit){
		//validate params
		if(!isValidRequest(request)){
			return new OperationsResponse(ResponseCodeEnum.INVALID_INPUT, ResponseCodeEnum.INVALID_INPUT.getDescription());
		}

		//validate account details
		BankAccount account = getDbService().getByCriteria(BankAccount.class, Restrictions.eq("accountNumber", request.getAccountNo()));
		if(account == null){
			return new OperationsResponse(ResponseCodeEnum.ACCOUNT_NOT_FOUND, ResponseCodeEnum.ACCOUNT_NOT_FOUND.getDescription());
		}

		double previousBal = account.getCurrentBalance();
		double newBal = 0.0;
		TransactionType txnType = null;
		if(deposit){
			newBal = previousBal + request.getAmount();
			txnType = TransactionType.CREDIT;
		}else{
			if(request.getAmount() > previousBal){
				return new OperationsResponse(ResponseCodeEnum.INVALID_INPUT, ResponseCodeEnum.INVALID_INPUT.getDescription());
			}
			newBal = previousBal - request.getAmount();
			txnType = TransactionType.DEBIT;
		}

		OperationsResponse resp = null;
		try{
			//log transaction
			TransactionLogs log = new TransactionLogs();
			log.setAccount(account);
			log.setAmount(request.getAmount());
			log.setNewBalance(newBal);
			log.setPrevBalance(previousBal);
			log.setTxnRef(request.getTxnRef());
			log.setTxnType(txnType);
			log.setTxnTime(new Date(request.getTxnTime()));
			save(log);
			
			//update account with new balance
			account.setCurrentBalance(newBal);
			update(account);
			
			resp = new OperationsResponse(ResponseCodeEnum.SUCCESS, ResponseCodeEnum.SUCCESS.getDescription());
		}catch(HibernateException e){
			resp = new OperationsResponse(ResponseCodeEnum.ERROR, ResponseCodeEnum.ERROR.getDescription());
		}
		
		return resp;
	}

	private boolean isValidRequest(BankOpRequest req){
		return StringUtils.isNotBlank(req.getAccountNo())
				&& StringUtils.isNotBlank(req.getTxnRef())
				&& req.getTxnTime() != null
				&& req.getAmount() > 0.0;
	}

}
