package com.an.banking.app.ws.ops;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.an.banking.app.ws.requests.BankOpRequest;

public interface IOperationsService {
	@POST
	@Path("/deposit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)	
	public Response depositMoney(BankOpRequest request);
	
	@POST
	@Path("/withdraw")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response withdrawMoney(BankOpRequest request);
}
