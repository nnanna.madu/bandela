package com.an.banking.app.ws.ops;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.an.banking.app.ws.requests.BankOpRequest;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 17:32:31
 */
@Path("/ops")
public class OperationsService implements IOperationsService {
	@Inject
	OperationsDataService dataService;

	@Override
	public Response depositMoney(BankOpRequest request) {
		OperationsResponse resp = dataService.logTransaction(request, true);
		return Response.ok(resp).build();
	}

	@Override
	public Response withdrawMoney(BankOpRequest request) {
		OperationsResponse resp = dataService.logTransaction(request, false);
		return Response.ok(resp).build();
	}

}
