/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.banking.app.ws;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 10:40:17
 */
public enum ResponseCodeEnum {

    SUCCESS(0, "Success"),
    ERROR(-1, "Error"),
    FAILED_AUTHENTICATION(-2, "Failed authentication"),
    INVALID_INPUT(-3, "Request contains missing or invalid fields"),
    INACTIVE_ACCOUNT(-4, "Inactive Account"),
    USER_NOT_FOUND(-5, "User not found"),
    ACCOUNT_NOT_FOUND(-6, "Account not found"),
    INVALID_PIN(-7, "Invalid PIN entered"),
    INVALID_TOKEN(-8, "Invalid token entered"),
    USER_ALREADY_EXISTS(-9, "User already exists");

    private ResponseCodeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    private int code;
    private String description;

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
