package com.an.banking.app.ws.accountopening;

import java.util.Date;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import com.an.banking.app.ds.DataService;
import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.app.ws.ResponseData;
import com.an.banking.app.ws.requests.NewAccountRequest;
import com.an.banking.model.entity.BankAccount;
import com.an.banking.model.entity.BankUser;
import com.an.banking.model.enums.AccountType;

@Stateless
public class NewAccountDataService extends DataService {

	/**
	 * Creates new {@link BankAccount} from the request
	 * @param req
	 * @return
	 */
	public ResponseData handleNewBankAccount(NewAccountRequest req){
		//validate compulsory fields
		if(!isValidRequest(req)){
			return new ResponseData(ResponseCodeEnum.INVALID_INPUT);
		}

		//validate the account type
		AccountType type = AccountType.from(req.getAccountType());
		if(type == null){
			return new ResponseData(ResponseCodeEnum.INVALID_INPUT);
		}

		//check if user already exists
		BankUser user = getDbService().getByCriteria(BankUser.class, Restrictions.eq("email", req.getEmail()));
		if(user != null){
			logger.debug("Exising user:" + user.getPk());
			return new ResponseData(ResponseCodeEnum.USER_ALREADY_EXISTS);
		}else{
			user = new BankUser();
			user.setAddress(req.getAddress());
			user.setBirthday(new Date(req.getBirthday()));
			user.setEmail(req.getEmail());
			user.setFirstName(req.getFirstName());
			user.setLastName(req.getLastName());
			user.setPhone(req.getPhoneNo());
			try{
				getDbService().create(user);
			}catch(ConstraintViolationException e){
				logger.error("Error in creating user: ", e);
				return new ResponseData(ResponseCodeEnum.ERROR);
			}catch(EJBTransactionRolledbackException e){
				logger.error("Error in creating user: ", e);
				return new ResponseData(ResponseCodeEnum.ERROR);
			}
		}
		BankAccount newAccount = new BankAccount();
		newAccount.setAccountNumber(req.getAccountNo());
		newAccount.setAccountType(type);
		newAccount.setCurrentBalance(0.0);
		newAccount.setPin(req.getPin());
		newAccount.setUser(user);

		try{
			getDbService().create(newAccount);
		}catch(ConstraintViolationException e){
			logger.error("Error in creating bank account: ", e);
			return new ResponseData(ResponseCodeEnum.ERROR);
		}

		return new ResponseData(ResponseCodeEnum.SUCCESS);
	}

	private boolean isValidRequest(NewAccountRequest req){
		return StringUtils.isNotEmpty(req.getAccountNo())
				|| StringUtils.isNotEmpty(req.getPin())
				|| StringUtils.isNotEmpty(req.getFirstName())
				|| StringUtils.isNotEmpty(req.getLastName())
				|| StringUtils.isNotEmpty(req.getAccountType())
				|| req.getBirthday() != null;
	}
}
