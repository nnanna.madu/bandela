package com.an.banking.app.ws.accountopening;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.an.banking.app.ws.ResponseData;
import com.an.banking.app.ws.requests.NewAccountRequest;

@Path("/new-account")
public class NewAccountService implements INewAccountService {
	
	@Inject
	NewAccountDataService dataService;

	@Override
	@PermitAll
	public Response createNewAccount(NewAccountRequest request) {
		ResponseData resp = dataService.handleNewBankAccount(request);
		return Response.ok(resp).build();
	}

}
