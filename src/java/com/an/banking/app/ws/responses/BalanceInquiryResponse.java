package com.an.banking.app.ws.responses;

import com.an.banking.app.ws.ResponseCodeEnum;
import com.an.banking.app.ws.ResponseData;

/**
 * 
 * @author Nnanna
 * @since 1 Oct 2017, 16:53:18
 */
public class BalanceInquiryResponse extends ResponseData {
	private double balance;
	
	public BalanceInquiryResponse(){}
	
	public BalanceInquiryResponse(ResponseCodeEnum code, String description){
		setCode(code);
		setDescription(description);
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
}
