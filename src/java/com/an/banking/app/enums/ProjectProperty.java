package com.an.banking.app.enums;

/**
 * App properties configured in the 
 * project config file (application.properties)
 * @author Nnanna
 * @since 30 Sep 2017, 17:59:16
 */
public enum ProjectProperty {
	
	ALLOWED_TOKENS("allowed-tokens", "123456,654321", "Comma-separated list of tokens allowed for validation"),
	JWT_SECRET_KEY("jwt-secret-key", "LASH12345#", "JWT secret key"),
	TOKEN_VALIDITY_PERIOD("token-validity-period", "30", "token validity period in minutes");
        
	private String key;
	private String defaultValue;
	private String description;

	private ProjectProperty(String key, String defaultValue, String description){
		this.key = key;
		this.defaultValue = defaultValue;
		this.description = description;
	}

	public String getKey() {
		return key;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getDescription() {
		return description;
	}
	
}