package com.an.banking.app.security;

/**
 * 
 * @author Nnanna
 * @since 30 Sep 2017, 20:32:36
 */
public class AccountAuth {
	private String user;
	private String accountNo;
	
	public AccountAuth(String user, String accountNo){
		this.user = user;
		this.accountNo = accountNo;
	}
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}
