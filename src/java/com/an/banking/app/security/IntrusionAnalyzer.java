package com.an.banking.app.security;

import java.util.Date;

import io.jsonwebtoken.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

import com.an.banking.app.enums.ProjectProperty;

import nw.commons.NeemClazz;

/**
 * 
 * @author Nnanna
 * @since 30 Sep 2017, 20:35:50
 */
@Singleton
public class IntrusionAnalyzer extends NeemClazz {
	private static final String AUTHORITIES_KEY = "auth";
	private String key;
	private int validityPeriod; //validity period in mins
	
	@PostConstruct
	public void init(){
		//fetch jwt params from props file
		key = appProps.getProperty(ProjectProperty.JWT_SECRET_KEY.getKey(), ProjectProperty.JWT_SECRET_KEY.getDefaultValue());
		validityPeriod = appProps.getInt(ProjectProperty.TOKEN_VALIDITY_PERIOD.getKey(), Integer.valueOf(ProjectProperty.TOKEN_VALIDITY_PERIOD.getDefaultValue()));
	}
	
	public String generateToken(String username, String accountNo){
		AccountAuth auth = new AccountAuth(username, accountNo);
		return createJwtToken(auth);
	}
	
	public String createJwtToken(AccountAuth auth){
		//convert validity to ms
		long validity = validityPeriod * 60 * 1000L;
		long now = System.currentTimeMillis();
		
		return Jwts.builder()
				.setSubject(auth.getUser())
				.claim(AUTHORITIES_KEY, auth.getAccountNo())
				.signWith(SignatureAlgorithm.HS512, key)
				.setExpiration(new Date(now + validity))
				.compact();
	}
	
	public boolean validateToken(String token){
		try{
			Jwts.parser().setSigningKey(key).parseClaimsJws(token);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature", e);
        } catch (MalformedJwtException e) {
        	logger.error("Invalid JWT token", e);
        } catch (ExpiredJwtException e) {
        	logger.error("Expired JWT token", e);
        } catch (UnsupportedJwtException e) {
        	logger.error("Unsupported JWT token", e);
        } catch (IllegalArgumentException e) {
        	logger.error("JWT token compact of handler are invalid", e);
        }
        return false;
	}
}
