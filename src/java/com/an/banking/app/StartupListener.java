package com.an.banking.app;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import nw.commons.NeemClazz;
import nw.orm.core.service.Nworm;

@Startup
@Singleton 
@TransactionManagement(TransactionManagementType.BEAN)
public class StartupListener extends NeemClazz {
	
	@PostConstruct
	public void start(){
		logger.info("Starting Bandela Services...");
        Nworm dbService = Nworm.getInstance();
		dbService.enableJTA();
		dbService.enableSessionByContext();
		logger.info("Bandela services started!");
	}
	
	@PreDestroy
	public void stop(){
		logger.info("Shutting down Bandela services...");
		Nworm.getInstance().closeFactory();
	}
}