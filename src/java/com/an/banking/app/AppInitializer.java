package com.an.banking.app;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.an.banking.app.ds.BankDS;

import nw.commons.NeemClazz;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 14:24:35
 */
@Singleton
@Startup
@DependsOn("StartupListener")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AppInitializer extends NeemClazz {
	@Inject
	BankDS ds;
	
	@PostConstruct
	protected void init(){
		ds.setupDefaultUser();
	}
}