package com.an.banking.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import nw.commons.BCrypt;
import nw.orm.core.IEntity;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.an.banking.model.enums.AccountType;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 04:51:18
 */
@Audited
@Entity
@Table(name = "bank_account")
public class BankAccount extends IEntity {
	@Column(name = "account_no", nullable = false, unique = true)
	private String accountNumber;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "account_type", nullable = false)
	private AccountType accountType;
	
	@Column(name = "pin")
	private String pin;
	
	@Column(name = "current_bal", nullable = false)
	private double currentBalance;
	
	@NotAudited
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_fk", nullable = false)
	private BankUser user;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = BCrypt.hashpw(pin, BCrypt.gensalt());
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public BankUser getUser() {
		return user;
	}

	public void setUser(BankUser user) {
		this.user = user;
	}
}
