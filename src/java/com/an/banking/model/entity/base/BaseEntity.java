package com.an.banking.model.entity.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 05:08:13
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
	private static final long serialVersionUID = -8883380838831582549L;
	
	@Id
	@GeneratedValue
	@Column(name = "ID", nullable = false, insertable = true, updatable = false)
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
