package com.an.banking.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import nw.orm.core.IEntity;

import org.hibernate.envers.NotAudited;

import com.an.banking.model.enums.TransactionType;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 04:50:44
 */
@Entity
@Table(name = "TXN_LOGS")
public class TransactionLogs extends IEntity {
	@NotAudited
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "account_fk", nullable = false)
	private BankAccount account;
	
	@Column(name = "txn_ref", nullable = false)
	private String txnRef;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "txn_type", nullable = false)
	private TransactionType txnType;
	
	@Column(name = "amount", nullable = false)
	private Double amount;
	
	@Column(name = "prev_bal", nullable = false)
	private Double prevBalance;
	
	@Column(name = "new_bal", nullable = false)
	private Double newBalance;
	
	@Column(name = "txn_charge")
	private Double txnCharge;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "txn_timestamp", nullable = false)
	private Date txnTime;

	public BankAccount getAccount() {
		return account;
	}

	public void setAccount(BankAccount account) {
		this.account = account;
	}

	public String getTxnRef() {
		return txnRef;
	}

	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}

	public TransactionType getTxnType() {
		return txnType;
	}

	public void setTxnType(TransactionType txnType) {
		this.txnType = txnType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getPrevBalance() {
		return prevBalance;
	}

	public void setPrevBalance(Double prevBalance) {
		this.prevBalance = prevBalance;
	}

	public Double getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(Double newBalance) {
		this.newBalance = newBalance;
	}

	public Double getTxnCharge() {
		return txnCharge;
	}

	public void setTxnCharge(Double txnCharge) {
		this.txnCharge = txnCharge;
	}

	public Date getTxnTime() {
		return txnTime;
	}

	public void setTxnTime(Date txnTime) {
		this.txnTime = txnTime;
	}

}
