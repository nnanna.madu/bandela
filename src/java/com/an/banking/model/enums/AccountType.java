package com.an.banking.model.enums;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 04:53:42
 */
public enum AccountType {
	CURRENT("CURRENT"),
	SAVINGS("SAVINGS"),
	FIXED("FIXED");
	
	private String type;

	AccountType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public static AccountType from(String type) {
        if (type != null) {
            for (AccountType b : AccountType.values()) {
                if (type.equalsIgnoreCase(b.getType())) {
                    return b;
                }
            }
        }
        return null;
    }
}
