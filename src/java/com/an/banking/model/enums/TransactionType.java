package com.an.banking.model.enums;

/**
 * 
 * @author Nnanna
 * @since 29 Sep 2017, 04:53:37
 */
public enum TransactionType {
	DEBIT,
	CREDIT;
}
